
//#region Requires

const request = require('request');                             // used to send https requests
const WebSocket = require('ws');                                // used to subscribe to web socket
const AppConstants = require('./appConstants.js');

//#endregion

//#region PureCloud org settings

const organizationId = "<YOUR PURECLOUD ORGANIZATION ID>";  // Your PureCloud Organization Id
const deploymentId = "<YOUR CHAT DEPLOYMENT ID>";           // Your PureCloud Chat Deployment Id
const queueName = "<YOUR QUEUE NAME>";                      // The name of the queue you want chats to be routed to
const env = 'mypurecloud.ie';                               // Your PureCloud environment (mypurecloud.com / mypurecloud.ie / mypurecloud.com.au / mypurecloud.de / mypurecloud.jp)

//#endregion /PureCloud org settings

// Start a chat
function startChat(storageObject) {

  return new Promise((resolve, reject) => {
    let myBody = {
      "organizationId": organizationId,
      "deploymentId": deploymentId,
      "routingTarget": {
        "targetType": "QUEUE",
        "targetAddress": queueName
      },
      "memberInfo": {
        "displayName": "Steve Jobs",
        "profileImageUrl": "https://banner2.kisspng.com/20181201/xes/kisspng-call-centre-customer-service-clip-art-computer-ico-homepage-vacuum-5c02fa4fe698b1.3075985415436990239445.jpg",
        "customFields": {
          "firstName": "Steve",
          "lastName": "Jobs"
        }
      }
    };

    let options = {
      url: 'https://api.' + env + '/api/v2/webchat/guest/conversations',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(myBody)
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);

        var webSocket = new WebSocket(info.eventStreamUri);
        webSocket.on('open', function () {
          //Connection is open. Start the subscription(s)
          console.log('WebSocket opened');
        });

        webSocket.on('message', function (message) {
          var data = JSON.parse(message);

          // We do not care about channel.metadata messages. Ignore them.
          if (data.topicName == 'channel.metadata') {
            return;
          }

          try {

            if (!storageObject.purecloud) {
              // do only once after chat session initated. Save particiapnt data and other required informations
              storageObject.purecloud = {};
              storageObject.purecloud.conversationId = data.eventBody.conversation.id;
              storageObject.purecloud.agentId = data.eventBody.member.id;
              //storageObject.purecloud.botConversationId = _context.conversation.id;
              storageObject.purecloud.chatId = info.jwt;

              console.log('conversationId:', storageObject.purecloud.conversationId);
              console.log('agentId:', storageObject.purecloud.agentId);

              // Send history
              //sendMessageToPureCloud(buildHistory(storageObject._conversationState.history), storageObject.purecloud);
              //sendMessageToPureCloud("Hello!!!", storageObject.purecloud);
              resolve(storageObject);
            }

            // new message from purecloud received
            if (data.metadata.type == 'message') {

              // We do not want to display message from the bot again (echo)
              // display only messages where senderId is different than current botId
              if (data.eventBody.sender.id != storageObject.purecloud.agentId) {
                console.log('msg from pc:', data.eventBody.body);
                sendMessageToBot(data.eventBody.body, storageObject);
              }
              // member-change event (detect DISCONNECT event)
            }
            else if (data.metadata.type == 'member-change' && data.eventBody.member.id == storageObject.purecloud.agentId && data.eventBody.member.state == 'DISCONNECTED') {
              console.log('# chat disconnected, clear bot session');
              storageObject.purecloud = undefined;
              sendMessageToBot('[purecloud disconnected]', storageObject);
              storageObject.disconnect();
            }

          } catch (error) {
            console.log(error);
            reject(error);
          }
        });
      }
    });
  });

}

// Send message to purecloud
function sendMessageToPureCloud(_msg, _data) {
  console.log('sendMessageToPureCloud:', _msg);

  let myBody = {
    body: _msg,
    bodyType: 'standard'
  };

  let options = {
    url: 'https://api.' + env + '/api/v2/webchat/guest/conversations/' + _data.purecloud.conversationId + '/members/' + _data.purecloud.agentId + '/messages',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + _data.purecloud.chatId
    },
    body: JSON.stringify(myBody)
  };

  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body);
      console.log('msg sent to pc:', _msg);

    } else {
      console.log(error);
    }
  });
}

// send message to the bot
function sendMessageToBot(_msg, storageObject) {
  storageObject.emit(AppConstants.EVENT_CUSTOMER_MESSAGE, _msg);
}

module.exports = {
  startChat,
  sendMessageToPureCloud
};
